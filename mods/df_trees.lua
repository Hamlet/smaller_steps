--[[
	Smaller Steps - Makes stairs and slabs use smaller shapes.
	Copyright © 2018-2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Procedure
--

local pr_StairsOverriders = function()

	-- Constants
	local t_nodesStairsNormal = {
		'stairs:stair_black_cap_wood', 'stairs:stair_blood_thorn_wood',
		'stairs:stair_fungiwood_wood', 'stairs:stair_goblin_cap_stem_wood',
		'stairs:stair_goblin_cap_wood', 'stairs:stair_nether_cap_wood',
		'stairs:stair_spore_tree_wood', 'stairs:stair_tower_cap_wood',
		'stairs:stair_tunnel_tube_wood'
	}

	local t_nodesStairsOuter = {
		'stairs:stair_outer_black_cap_wood',
		'stairs:stair_outer_blood_thorn_wood',
		'stairs:stair_outer_fungiwood_wood',
		'stairs:stair_outer_goblin_cap_stem_wood',
		'stairs:stair_outer_goblin_cap_wood',
		'stairs:stair_outer_nether_cap_wood',
		'stairs:stair_outer_spore_tree_wood',
		'stairs:stair_outer_tower_cap_wood',
		'stairs:stair_outer_tunnel_tube_wood'
	}

	local t_nodesStairsInner = {
		'stairs:stair_inner_black_cap_wood',
		'stairs:stair_inner_blood_thorn_wood',
		'stairs:stair_inner_fungiwood_wood',
		'stairs:stair_inner_goblin_cap_stem_wood',
		'stairs:stair_inner_goblin_cap_wood',
		'stairs:stair_inner_nether_cap_wood',
		'stairs:stair_inner_spore_tree_wood',
		'stairs:stair_inner_tower_cap_wood',
		'stairs:stair_inner_tunnel_tube_wood'
	}

	local t_nodesSlabs = {
		'stairs:slab_black_cap_wood', 'stairs:slab_blood_thorn_wood',
		'stairs:slab_fungiwood_wood', 'stairs:slab_goblin_cap_stem_wood',
		'stairs:slab_goblin_cap_wood', 'stairs:slab_nether_cap_wood',
		'stairs:slab_spore_tree_wood', 'stairs:slab_tower_cap_wood',
		'stairs:slab_tunnel_tube_wood'
	}

	for i_element = 1, #t_nodesStairsNormal do
		smaller_steps.pr_NodeOverrider(t_nodesStairsNormal[i_element], 'normal')
	end

	for i_element = 1, #t_nodesStairsOuter do
		smaller_steps.pr_NodeOverrider(t_nodesStairsOuter[i_element], 'outer')
	end

	for i_element = 1, #t_nodesStairsInner do
		smaller_steps.pr_NodeOverrider(t_nodesStairsInner[i_element], 'inner')
	end

	for i_element = 1, #t_nodesSlabs do
		smaller_steps.pr_NodeOverrider(t_nodesSlabs[i_element], 'slab')
	end
end


--
-- Main body
--

pr_StairsOverriders()
