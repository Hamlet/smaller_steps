# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/).


## [Unreleased]

	- No further features planned.


## [1.4.0] - 2020-06-22
### Added

	- Support for df_trees, df_underworld_items ("DF Style Caverns" nodes).

### Changed

	- Licensed changed to EUPL v1.2 or later.
	- Code rewritten from scratch to be as much "memory saving" as possible.

### Removed

	- Support for Minetest 4.x


## [1.3.0] - 2019-09-14
### Added

	- Support for Minetest Game v5.0.1 new slabs and stairs.
	- Support for my_door_wood.

### Modified

	- Outer and inner corners enabled by default.
	- Corners' .lua files merged with normal shapes' .lua files.

### Removed

	- Option to toggle stairs corners.


## [1.2.4] - 2018-05-21
### Modified

	- Mod rearranged to follow Minetest-Mods manifesto's guidelines.

### Removed

	- ../doc/


## [1.2.3] - 2018-04-21
### Added

	- ../doc/

### Changed

	- Node overriders now use the "for" cycle, where possible, this makes
		easier to read and mantain the code.
