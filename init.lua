--[[
	Smaller Steps - Makes stairs and slabs use smaller shapes.
	Copyright © 2018-2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Global mod's namespace
--

smaller_steps = {}


--
-- Procedure
--

-- Subfiles loader
local pr_LoadSubFiles = function()

	-- Constants
	local s_MOD_PATH = minetest.get_modpath('smaller_steps')

	local s_CASTLE_MASONRY = minetest.get_modpath('castle_masonry')
	local s_DARKAGE = minetest.get_modpath('darkage')
	local s_DF_TREES = minetest.get_modpath('df_trees')
	local s_DF_UNDERWORLD_ITEMS = minetest.get_modpath('df_underworld_items')
	local s_MY_DOOR_WOOD = minetest.get_modpath('my_door_wood')
	local s_STAIRS = minetest.get_modpath('stairs')

	-- Body
	dofile(s_MOD_PATH .. '/procedures.lua')

	if (s_CASTLE_MASONRY ~= nil) then
		dofile(s_MOD_PATH .. '/mods/castle_masonry.lua')
	end

	if (s_DARKAGE ~= nil) then
		dofile(s_MOD_PATH .. '/mods/darkage.lua')
	end

	if (s_DF_TREES ~= nil) then
		dofile(s_MOD_PATH .. '/mods/df_trees.lua')
	end

	if (s_DF_UNDERWORLD_ITEMS ~= nil) then
		dofile(s_MOD_PATH .. '/mods/df_underworld_items.lua')
	end

	if (s_MY_DOOR_WOOD ~= nil) then
		dofile(s_MOD_PATH .. '/mods/my_door_wood.lua')
	end

	if (s_STAIRS ~= nil) then
		dofile(s_MOD_PATH .. '/mods/stairs.lua')
	end
end


--
-- Main body
--

pr_LoadSubFiles()

smaller_steps.pr_LogMessage()

smaller_steps = nil -- Flush the table to save memory.
